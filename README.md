# Goal Server <!-- omit in toc -->
The Goal Server is written in arduino/C++ specifically for the esp32. Whenever a goal is scored the esp32 wakes up from deep sleep and tries to send its information for a few seconds to the Pi. When it gets the confirmation that the data has been received or it times out it goes back into deep sleep.

## [Table of Contents](#table-of-contents)
- [Table of Contents](#table-of-contents)
- [Architecture](#architecture)
  - [Software management](#software-management)
  - [Communication of IoT Devices](#communication-of-iot-devices)
  - [ESP32 Power Modes](#esp32-power-modes)
  - [Bluetooth Low Energy](#bluetooth-low-energy)
    - [Deep Sleep vs Hibernation](#deep-sleep-vs-hibernation)
- [Coding practices](#coding-practices)
- [Measurements](#measurements)
- [Test](#test)
  - [Future](#future)
- [Saving Goals](#saving-goals)
- [Alive and updates](#alive-and-updates)
- [The Repo](#the-repo)
- [Some Infos about the Hardware](#some-infos-about-the-hardware)
- [Ubuntu](#ubuntu)
  - [VS Code Formatting and auto-completion error](#vs-code-formatting-and-auto-completion-error)

## [Architecture](#architecture)
Because a pitcure explains more than a thousand words: <br />
<img src="pictures/architecture.png" width="400" style="margin:20px" /> <br />
### Software management
Management of the software is done in the cloud and then automatically synchronized between the cloud master and the edge controller. Currently, the edge part is running a full fledged K8S solution, but the plan is to switch something a lot smaller, e.g. K3S which has a 40Mb (compressed) binary. Deployments are all done via Containers and we can reap all the benefits of using K8S, i.e. rolling deployments, health checks, networking. A update for the esp32 is build on the client (or server) inside a container. The container is then deployed on the Pi where it sends the data to the esp32. When the esp32 sends the correct/new version in its beacon the container stops. (Maybe: For updates the esp32 enables a full bluetooth connection.)
<br />

### [Communication of IoT Devices](#communication-of-iot-devices)
The Pi and esp32 communicate via BLE, where the esp32 is the Server and the Pi is the client. The BLE is in deep sleep for most of the time and only woken up by a new goal (see Future for battery consideration.). 

### [ESP32 Power Modes](#esp32-power-modes)
The esp32 has five different power modes: *Active*, *Modem Sleep*, *Light Sleep*, *Deep Sleep* and *Hibernation*. To transmit data the antena needs to be enabled and there is only one mode for this *active*, shown in the next picture.
<br/>**Active** <br />
<img src="pictures/esp32Active.png" width="400px" style="margin-left:20px" /> <br />
Courtesy of https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/ <br/> <p></p>
In *active* wifi and bluetooth are both enabled. Turning off wifi could thus lead to further battery improvements and is going to be tested later. <br/>
The intend of the of project is to have a long-lifed  battery powered system, so conseving battery while the device is idle is crucial. The esp32 offers two power modes which operate at very low power consumption and are suitable for this projects purpose as they are both able to wake up from a GPIO input.
<br/>**Deep Sleep** <br />
<br/><img src="pictures/esp32DeepSleep.png" width="400px" style="margin-left:20px" /> <br />
Courtesy of https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/ <br/> <p></p>
*Deep sleep* is the preferred power saving mode because the esp32 is still able to measure and analyze sensor data, do low computation tasks and it has a tiny memory to save variable states. This mode is also called *ULP sensor-monitored pattern* as the device can measure sensor data and react to it. An example could be that the device only wakes up on every third button click and then sends data over bluetooth.
<br/>**Hibernate** <br />
<br/><img src="pictures/esp32Hibernate.png" width="400px" style="margin-left:20px" /> <br />
Courtesy of https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/ <br/> <p></p>
Lastly, *hibernate* disables the ULP as well and only leaves the RTC enabled. The device can be thus woken up by time or certain GPIO input, but no logic can be performed before waking the device up.

https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/system/sleep_modes.html


### Bluetooth Low Energy
Ble is a fairly recent extension to the bluetooth technology stack, introduced in 2009 with bluetooth 4.0, and allows the transfer of data between devices with fairly low energy consumption.  The data is transferred inside a beacon. Is it? How does Ble even work because there is actually a connection establishment.


#### Deep Sleep vs Hibernation
Hibernation offers even more power savings, up to 4 times more (which is a difference of 7.5 microA...), but it is not really documented by espessif (maker of esp32).<br />
So for the moment we are sticking with deep sleep, but are looking into hibernation.

## Coding practices
- intialize on demand. for example bluetooth is only initialized when needed.

## Measurements
* How much power does it take to act as server
* How much power does it take to act as client
* How much power do the lasers and sensors consum if they are always on.

## Test
What we test:
* polling vs wake up
* --> power consumption overhead of 
* 
Battery consumption is the main reason for the current design and has been tested against competing options. The two different wireframe designs can be seen in figure ** and figure **, repectively. The first test case scenario pits the powerconsumption of polling vs listening for a goal to wake up the device. e ensure that the tests the  same time the device is  are performt in such 


### [Future](#future)
There are two approaches for handling the sensors: <br />
 - [x] Let the laser and sensor run all and they wake up the esp32.
 - [ ] The esp32 controls the laser/sensor with a mosfet. It has a game mode where it is polling the Pi, with a polling frequency of 10 seconds during rush hour. If there is a game, the esp32 goes into game mode. <br />

<!-- https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/ -->
For more information about power states on the esp32 

## Saving Goals
The esp32 only tries to send a goal for 5 seconds, when it can reach the Pi it goes back into deep sleep and forgets about the goal. This is to keep it simple and not check states after wake ups. <br />
*Not implemented:* On the Pi we save the state of a current game until either it reaches the cloud again or the game is over. This is so that we don't have to check times between games in the cloud and onprem. 


## [Alive and updates](#polling-alive-and-updates)
The esp32 connects to the Pi once a while to ask for updates and tell it that is still working normally. *Not implemented:* If no update for a while, the Pi can send of an alarm for the sys admin to check the Pi. <br />
In predefined updates (maybe 30 minutes) the esp32 wakes itself up and polls the raspberry for updates, to synch time and maybe more stuff in the future.
*Not implemented:* OTA updates are possible with the esp32 but the memory is limitted make it (almost) impossbile to use with the buetooth module.

## The Repo
https://gitlab.com/iaexp/iaf/lightsensors


## Some Infos about the Hardware
https://circuitdigest.com/microcontroller-projects/getting-started-with-esp32-with-arduino-ide

## Ubuntu

### VS Code Formatting and auto-completion error
install libtinfo5 for code completion and formatting to work <br />
``` sudo apt install libtinfo5 ```