#include <Arduino.h>

int MOSFET=4;

void setupConfigs()
{
  Serial.begin(115200);
  Serial.println("******* New:   ********");

}

void setup()
{
  setupConfigs();
  pinMode(MOSFET, OUTPUT);
}

// Start stop
void loop()
{
  digitalWrite(MOSFET,HIGH);
  Serial.println("High");
  delay(1000);
  digitalWrite(MOSFET,LOW);
  Serial.println("LOW");
  delay(1000);
}