#include <Arduino.h>

void setupConfigs()
{
  Serial.begin(115200);
  Serial.println("******* New:   ********");
}

void setup()
{
  setupConfigs();
}

// Start stop
void loop()
{
  int repitions = 1000;
  for (int c = 0; c < repitions; c++)
  {
  sendGoal();
  Serial.println("******* DELAY  ********");
  delay(10000);
  Serial.println("******* WAKEUP  ********");
  }
}

// sendGoal opens a bleServer for a device (the Pi) to connected
// it uses indicate to send the data
// it makes three attempts of sending
void sendGoal()
{
  int c = 0;
  bool connected = false;
  setupBleServer();
  int connAtt = 3 // make connectionAttempts attempts

      // wait for client to connect
      while (c < connAtt)
  {
    if (!deviceConnected())
    {
      c++;
    }
    else
    {
      connected = true;
      c = 0;
    }
  }