#include <Arduino.h>

void setupConfigs()
{
  Serial.begin(115200);
  Serial.println("******* New:   ********");
}

void setup()
{
  setupConfigs();
}

// Start stop
void loop()
{
  int repitions = 1000;
  for (int c = 0; c < repitions; c++)
  {
  deepSleep(10000,true);
  Serial.println("******* DELAY  ********");
  delay(10000);
  Serial.println("******* WAKEUP  ********");
  }
}

// deepSleep puts the device into deep sleep.
// i is in Millis.
// First checks if it should wakeupOnSignal and time
// If either time is <= 0 or wakeupOnSignal==false it discards the value
void deepSleep(int time, bool wakeupOnSignal)
{
  Serial.println("******* DEEPSLEEP  ********");

  esp_sleep_enable_timer_wakeup(time * 1000);
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, HIGH);
  esp_deep_sleep_start();

  Serial.println("******* WAKE UP from DEEPSLEEP  ********");
}
