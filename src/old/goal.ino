// sendGoal opens a bleServer for a device (the Pi) to connected
// it uses indicate to send the data
// it makes three attempts of sending
void sendGoal()
{
  int c = 0;
  bool connected = false;
  setupBleServer();
  int connAtt = 3; // make connectionAttempts attempts

      // wait for client to connect
      while (c < connAtt)
  {
    if (!deviceConnected)
    {
      c++;
    }
    else
    {
      connected = true;
      c = 0;
    }
  }

  // try to send goals via indicate
  // this starts when devices are connected, so 1 second should be more than enough for ACK
  int sendAtt = 3;
   while (connected && !acknowledged)
  {
    sendGoalMsg = score + "1}";
    pCharacteristic->setValue(sendGoalMsg);
    pCharacteristic->indicate();
    pCharacteristic->setCallbacks(new SendGoalCallback());
    // min delay is 30ms if not esp32 sends beacons all the time overflowing itself.
    delay(200);
    c++;
    if (acknowledged)
    {
      Serial.println("stopping connection");
      acknowledged = false;
      deepSleep(10000, true);
      // goal=false;
      // sendGoalMsg ="";
      return;
    }
  }
}

class SendGoalCallback : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string rxValue = pCharacteristic->getValue();
    if (rxValue.length() > 0)
    {
      checkACK();
    }
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
  }
};

// Check if send goal was acknowledged
void checkACK()
{
  if (rxValue == sendGoalMsg)
  {
    acknowledged = true;
  }
}