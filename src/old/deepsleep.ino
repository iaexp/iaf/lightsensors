// deepSleep puts the device into deep sleep.
// i is in Millis. 
// First checks if it should wakeupOnSignal and time
// If either time is <= 0 or wakeupOnSignal==false it discards the value
void deepSleep(int time, bool wakeupOnSignal)
{
  if (wakeupOnSignal && time > 0)
  {
    esp_sleep_enable_timer_wakeup(time * 1000);
    esp_deep_sleep_start();
  } else if (time<=0)
  {
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_33,HIGH);
    esp_deep_sleep_start();
  } else
  {
    esp_sleep_enable_timer_wakeup(time * 1000);
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_33,HIGH);
    esp_deep_sleep_start();
  }
  esp_deep_sleep_start();
}