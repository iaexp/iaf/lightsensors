#include <Arduino.h>

int BTN = 21;
bool btnPressed = true;
int buttonState = 0;
int MOSFET = 23;

// IAF defs
bool scored = true;
bool acknowledged = false;
std::string score = "{\"speed\": ";
std::string sendGoalMsg = "";
// SendOptinos with
enum SendOptions
{
  GOAL,
  ACTIVE
};
SendOptions sendOption = ACTIVE;

// UPDATE stuff
bool update = false;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

void setupPins()
{
  pinMode(BTN, INPUT);
  //  init mosfet for Sensor power control
  pinMode(MOSFET, OUTPUT);
  digitalWrite(MOSFET, HIGH);
}
void setupConfigs()
{
  Serial.begin(115200);
  Serial.println("******* New:   ********");
  // Deactivate radio and wifi
  // WiFi.mode( WIFI_OFF );
}

void setup()
{
  setupConfigs();
  setupPins();
}

// Laser is hitting: digitalRead(sensor)==1
void loop()
{
  if (scored)
  {
    sendGoal();
  }
}
