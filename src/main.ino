/*
    Video: https://www.youtube.com/watch?v=oCMOYS71NIU
    Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleNotify.cpp
    Ported to Arduino ESP32 by Evandro Copercini
    updated by chegewara

   Create a BLE server that, once we receive a connection, will send periodic notifications.
   The service advertises itself as: 4fafc201-1fb5-459e-8fcc-c5c9c331914b
   And has a characteristic of: beb5483e-36e1-4688-b7f5-ea07361b26a8

   The design of creating the BLE server is:
   1. Create a BLE Server
   2. Create a BLE Service
   3. Create a BLE Characteristic on the Service
   4. Create a BLE Descriptor on the characteristic
   5. Start the service.
   6. Start advertising.

   A connect hander associated with the server starts a background task that performs notification
   every couple of seconds.
*/
#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>


// #include <WiFi.h>

BLEServer *pServer = NULL;
BLECharacteristic *pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;

int BTN = 22;
bool btnPressed = true;
int buttonState = 0;
int MOSFET = 23;

// IAF defs
std::string BLE_NAME = "RED_GOAL";
bool goal = true;
bool acknowledged = false;
std::string score = "{\"speed\": ";
std::string sendGoalMsg = "";
// SendOptinos with
enum SendOptions
{
  GOAL,
  ACTIVE
};
SendOptions sendOption = ACTIVE;

// UPDATE stuff
bool update = false;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_NAME "GOAL_SERVICE"
#define SERVICE_UUID "298ed54e-6b19-11e9-a923-1681be663d3e"
#define CHARACTERISTIC_NAME "GOAL_CHARACTERISTICS"
#define CHARACTERISTIC_UUID "298ed8d2-6b19-11e9-a923-1681be663d3e"


int MOSFET = 17;
int SENSOR = 4;
int i = 0;

void setup()
{
    setupBle();
    pinMode(SENSOR, INPUT);
    pinMode(MOSFET, OUTPUT);
    Serial.begin(115200);
    Serial.println("***** New:   ******");
}


class MyServerCallbacks : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    deviceConnected = true;
  };

  void onDisconnect(BLEServer *pServer)
  {
    deviceConnected = false;
  }
};

class SendGoalCallback : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string rxValue = pCharacteristic->getValue();
    if (rxValue.length() > 0)
    {
      Serial.print("***** Received Value: ");
      for (int i = 0; i < rxValue.length(); i++)
      {
        Serial.print(rxValue[i]);
      }
      if (rxValue == sendGoalMsg)
      {
        acknowledged = true;
      }
      
    }
  }
  void onRead(BLECharacteristic *pCharacteristic)
  {
  }
};

void setupBle()
{
  Serial.begin(115200);
  Serial.println("*** New:   ****");

  // Deactivate radio and wifi
  // WiFi.mode( WIFI_OFF );

  //  init mosfet for Sensor power control
  pinMode(BTN, OUTPUT);
  pinMode(MOSFET, OUTPUT);

  // Create the BLE Device
    // Create the BLE Device
  BLEDevice::init(BLE_NAME);
  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());
  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);
  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID,
      BLECharacteristic::PROPERTY_WRITE |
          BLECharacteristic::PROPERTY_INDICATE);

  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());
  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x00); // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to indicate...");
}

void loop()
{
  if (!deviceConnected && oldDeviceConnected)
  {
    delay(500);                  // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  if (deviceConnected && !oldDeviceConnected)
  {
    // do stuff here on connecting
    /*
    if (goal)
    {
      sendGoal();
    }
    if (update) {

    }
    */
   digitalWrite(MOSFET, LOW);
    i = digitalRead(SENSOR);
    Serial.println(i);
    if (i == 0)
    {

        Serial.println("i is 0");
        digitalWrite(MOSFET, HIGH);
        Serial.println("LOW");
        sendGoal();
        delay(1000);
        digitalWrite(MOSFET, LOW);
        delay(10);
    }
    else
    {
        Serial.println("i is 1");
    }
    oldDeviceConnected = deviceConnected;
  }
}


// i is in Millis
void deepSleep(int time)
{
  Serial.println("**** going into deep sleep for: " + String(time) + " *****");
  esp_sleep_enable_timer_wakeup(time * 1000);
  esp_deep_sleep_start();
  Serial.println("**** I woke up with time: " + String(time) + " *****");
}

void sendGoal()
{
  int counter = 0;
  while (!acknowledged)
  {
    sendGoalMsg = score + "1}";
    pCharacteristic->setValue(sendGoalMsg);
    pCharacteristic->indicate();
    pCharacteristic->setCallbacks(new SendGoalCallback());

    Serial.println("indicate");

    // min delay is 30ms if not esp32 sends beacons all the time overflowing itself.
    // this starts when devices are connected, so 1 second should be more than enough for ACK
    delay(200);
    counter++;
    if (acknowledged || counter > 5)
    {
      Serial.println("stopping connection");
      acknowledged = false;
      deepSleep(5000);
      // goal=false;
      // sendGoalMsg ="";
      return;
    }
  }
}